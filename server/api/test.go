package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type Test struct {}

func (t Test) Setup(r *gin.RouterGroup) {
	r.GET("/", func(c *gin.Context) {
		c.String(http.StatusOK, "hi there")
	})
}
