package main

import (
	"indent.cloud/gatekeeper/api"
	"net/http"

	"github.com/gin-gonic/gin"
)

func setupRouter() *gin.Engine {
	r := gin.Default()

	// Ping test
	r.GET("/ping", func(c *gin.Context) {
		c.String(http.StatusOK, "pong")
	})

	apiRouter := r.Group("/api/v1")
	
	testAPI := api.Test{}  
	testAPI.Setup(apiRouter.Group("/test"))

	return r
}

func main() {
	r := setupRouter()

	r.Run(":3000")
}
